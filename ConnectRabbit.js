var amqp = require('amqplib/callback_api');

const amqpServer = "amqp://admin:admin@localhost:5672";

const connection = () => new Promise((resolve, reject) => {
  amqp.connect(amqpServer, (err, conn) => {
    if (err) return reject(err);
    return resolve(conn);
  })
})

module.exports = connection;