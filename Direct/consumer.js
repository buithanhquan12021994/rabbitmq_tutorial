const connection = require('../ConnectRabbit');

const typeDirectName = process.argv.slice(2); // from input console
// example: node consumer.js warning info

async function consumer() {
  try {
    let connect = await connection();
    // create channel
    let channel = await connect.createChannel();

    // 1. check that exchange is exist or not
    const exchangeName = 'log_type_message'
    await channel.assertExchange(exchangeName, 'direct', { durable: false }); // type exchange: direct
    
    // 2. Check the queue is exists or not
    const q = await channel.assertQueue('', { exclusive: true });

    // 3. bind queue with queueName, exchangeName, routingKey
    typeDirectName.forEach((routingKey) => {
      channel.bindQueue(q.queue, exchangeName, routingKey);
    })

    // 4. consume
    channel.consume(q.queue, function (msg) {
      console.table({
        Listening: typeDirectName.join(','),
        Exchange: msg.fields.exchange,
        RoutingKey: msg.fields.routingKey
      });
      if (msg.content) {
        console.log(`Message: ${msg.content.toString()}`);
      }
    })
  } catch (err) {
    console.log(err);
    throw err;
  }
}

consumer();