const connection = require('../ConnectRabbit');

const alertType = ['error', 'success', 'warning', 'info'];

let generateMessage = () => {
  let typeLog = Math.floor(Math.random() * alertType.length);
  return [
    alertType[typeLog],
    `[${alertType[typeLog]}] Here is message from log system`
  ];
}

async function producer() {
  try {
    let connect = await connection();
    // create channel
    let channel = await connect.createChannel();

    // 1. check that exchange is exist or not
    const exchange = 'log_type_message'
    await channel.assertExchange(exchange, 'direct', { durable: false }); // type exchange: direct
    // publish message to exchange with routing key
    setInterval(() => {
      let [routingKeyType, message] = generateMessage();
      channel.publish(exchange, routingKeyType, Buffer.from(message)); // send message to exchange_name with the binding_key and message
      console.log(`[${routingKeyType}]: ${message}`)
    }, 2000)
  } catch (err) {
    console.log(err);
    throw err;
  }
}

producer();