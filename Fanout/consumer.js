const connection = require('../ConnectRabbit');

async function consumer() {
  try {
    const connect = await connection();

    const channel = await connect.createChannel();

    // kiểm tra exchange name đã tồn tại hay chưa với gia trị type exchange, durable = false  <=> ko lưu xuong disk
    const exchangeName = 'logs';

    await channel.assertExchange(exchangeName, 'fanout', { durable: false });

    let q = await channel.assertQueue('', { exclusive: true }); // exclusive: true <=> khi ngắt connect ==> delete queue 
    channel.bindQueue(q.queue, exchangeName, ''); // nếu exchange là loại fanout thì option 3 có thể là bất cứ input gì
    channel.consume(q.queue, function (msg) {
      if (msg.content) {
        console.log('Msg: %s', msg.content.toString());
      }
    }, {
      noAck: true
    })
// channel.assertQueue('', { exclusive: true }, function (err, q) {
//   console.log('Waiting for message in %s. To exit process CTRL+C', q.queue);

//   channel.bindQueue(q.queue, exchangeName, ''); // bind queue to exchange with routing-key-name is empty '' <=> all

//   channel.consume(q.queue, function (msg) {
//     if (msg.content) {
//       console.log('Msg: %s', msg.content.toString());
//     }
//   }, {
//     noAck: true
//   })

// }); // exclusive: true <=>

    // craete channel
    // connect.createChannel(function (err, channel) {
    //   if (err) throw err;

    //   // assert exchange
    //   const exchangeName = 'logs';

    //   channel.assertExchange(exchangeName, 'fanout', { durable: false });

    //   channel.assertQueue('', {
    //     exclusive: true
    //   }, function (err, q) {
    //     if (err) throw err;

    //     console.log('Waiting for message in %s. To exit process CTRL+C', q.queue);

    //     channel.bindQueue(q.queue, exchangeName, ''); // nói rabbit mq server rằng send message to queue 

    //     channel.consume(q.queue, function (msg) {
    //       if (msg.content) {
    //         console.log('Msg: %s', msg.content.toString());
    //       }
    //     })
    //   }, {
    //     noAck: true
    //   })
    // })
  } catch (err) {
    throw err;
  }
} 

consumer();