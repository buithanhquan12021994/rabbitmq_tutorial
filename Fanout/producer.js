const connection = require('../ConnectRabbit');
const { generateDot } = require('../helpers');

async function producer() {
  try {
    const connect = await connection();

    const channel = await connect.createChannel();

    // exchangeName 
    const exchangeName = 'logs';

    // kiểm tra exchange đã tồn tại hay chưa, type: loại exchange (fanout, topic, header, direct)
    channel.assertExchange(exchangeName, 'fanout', { durable: false });

    // khởi tạo queue với random queue name, exclusive: tự động xóa queue nếu ko có consumers nào đang lắng nghe
    // channel.assertQueue('', { exclusive: true })

    setInterval(() => { 
      let msg = generateDot('Logs');
      // publish message thông qua random queue name hay all queue

      channel.publish(exchangeName, '', Buffer.from(msg));
      console.log("[x] Sent %s", msg);
    }, 1000)
   
  } catch (err) {
    console.log(err)
    throw err;
  }
}

producer();