const connnectionRabbitMq = require('../ConnectRabbit');


const args = process.argv.slice(2);

const [topicRoutingKey] = args;

const exchangeName = 'header_exchange';

async function consumer() {
  try {
    // 1. connect to rabbitmq server
    const connect = await connnectionRabbitMq();
    // 2. create a channel
    const channel = await connect.createChannel();
    // 3. check the exchange is exist or not
    await channel.assertExchange(exchangeName, 'headers', { durable: false });

    // 4. check the queue is exist or not
    const q = await channel.assertQueue('', { exclusive: false });
    // bind queue
    channel.bindQueue(q.queue, exchangeName, '', {
      'account': 'old', 'method': 'github', 'x-match': 'all'
    });

    // consume message
    channel.consume(q.queue, function (msg) {
      console.log(msg)
      if (msg.properties.headers) {
        console.table({
          Headers: JSON.stringify(msg.properties.headers),
          Message: JSON.parse(msg.content)
        })
      }
    })

  } catch (err) {
    console.log(err)
  }
}

consumer();