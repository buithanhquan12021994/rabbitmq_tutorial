const connectRabbit = require('../ConnectRabbit');

const exchange_name = 'header_exchange';

const [ msg ] = process.argv.slice(2);

const message = msg || 'text message from the header'

async function procedure() {
  try {
    // connect to rabbit server
    const connect = await connectRabbit();
    // create a channel
    const channel = await connect.createChannel();
    // check exchange is available or not
    await channel.assertExchange(exchange_name, 'headers', { durable: false }); // not store to disk

    // send message
    channel.publish(exchange_name, 
      '', 
      Buffer.from(JSON.stringify({
        user: 'btq',
        age: 28
      })), 
      {headers:{account: 'old', method: 'github'}}
    )

    console.log('Send the msg: ' + message)

    setTimeout(() => {
      connect.close();
      process.exit(0);
    }, 500)

  } catch (error) {
    console.log(error)
  }
} 


procedure()