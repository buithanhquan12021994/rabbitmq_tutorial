const connection = require('../ConnectRabbit');

var queue = 'hello_word';
var message = 'hello';


const consummer = async () => {
  try {
    const connect = await connection();
    connect.createChannel(function (err, channel) {
      if (err) throw err;

      channel.assertQueue(queue, { durable: false});


      channel.consume(queue, (msg) => {
        console.log(msg.content.toString());
      },{
        noAck: true
      });
    });

  } catch (err) {
    console.error('Error in connection rabbit_server')
    console.log(err)
  }
}

consummer()