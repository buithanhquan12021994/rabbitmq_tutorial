const connection = require('../ConnectRabbit');

var queue = 'hello_word';
var message = 'hello';


const procedure = async () => {
  try {
    const connect = await connection();

    let sendingMsg = null;

    connect.createChannel(function (err, channel) {
      if (err) throw err;
      
      // define the existing queue
      channel.assertQueue(queue, {
        durable: false,
      });

      sendingMsg = setInterval(() => {
        channel.sendToQueue(queue, Buffer.from(message));
        console.log('Already sent message')
      }, 1000)
    })  ;

    setTimeout(() => {
      clearInterval(sendingMsg);
      connect.close();
      console.log('Exit')
      process.exit(0)
    }, 10000)
  } catch (err) {
    console.error('Error in connection rabbit_server')
    console.log(err)
  }
}

procedure();