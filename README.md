
**Procedure**: is only sending.

**Consumer**: a similar meaning to receiving.

**An application can be both a producer and a consumer**.

### **Nodejs rabbitmq npm: [amqplib](https://amqp-node.github.io/amqplib/)**

# Task Queue

### **Producer**
If you want keep 'task' that is sent from the producer to the consumer in message broker, you should define the option in `channel.sendToQueue(queueName, { persistent: true, durable: true})` **[procedure]** 

If you shut down rabbitmq server with `persistent: false` => it will remove the channel and also the message

### **Consumer**

Like **Producer**, you should also use `durable: true`, to get the message. Inside the consumer, you should set the `noAck: false` (the message after is received by consumer should be replied to the rabbit-server that 'hey! I finish this task, you should remove this message in server')

``` 
channel.consume(queueName, function (msg) {
  // do something with the message
  // reply to the rabbit-server to remove this message
   channel.ack(msg);
  }, {
  noAck: false
})
```

If you want to control the RabbitMQ does not send more message at the sanme time to the consumer (when the consumer is busy to do task), use the `prefetch(number)` -  This tells RabbitMQ not to give more than 'number' messages to a worker at a time. 

Ex: `channel.prefetch(1)` => This tells RabbitMQ not to give more than one message to a worker at a time. 


# Fanout exchange 

### Folder: **./Fanout**

Fanout is a broadcast channel, will type 'Fanout', the exchange can understand that it will send a broadcast message for all the channel that is binded from consumer

### **Producer**

We don't care about the queue name anymore, the one thing we should care that the exchange name and the type should be 'Fanout'. After defining the exchange, we only publish messages to that exchange with the routing key empty ''

Example
```
// connect to RabbitMQ server
const connect = await connectRabbit(...);

// 1. Create a channel to rabbit server
const channel = await connect.createChannel();

// 2. Check the exchange is created or not. 
await channel.assertExchange(<exchange_name>, 'fanout', { durable: false }); // durable = false <=> dont want to store in disk

// 3. Publish the message to exchange
channel.publish(<exchange_name>, '', Buffer.from(<message>)); 
```

`channel.publish(<exchange_name>, <routing_key>, <content>, <option?>)`

<routing_key> now is empty. With the **Fanout** exchange, <routing_key> here is rejected and is not cared

### **Consumer**

The one thing that consumers should care is a binding queue. Consumers should bind the queue to the correct exchange name, with the correct exchange name they can get the message from the queue.

Example
```
// connect to RabbitMQ server
const connect = await connectRabbit(...);

// 1. Create a channel
const channel = await connect.createChannel();

// 2. Check the exchange is exists or not with the exchange type
await channel.assertExchange(<exchange_name>, 'fanout', { durable: false }); // dont store to disk

// 3. Check queue is exists or not
const q = channel.assertQueue('', { exclusive: true }); // exclusive: true - meaning that tell the rabbit should remove the queue if no consumer is active or die  

// 4. Binding queue to exchange
channel.bindQueue(q.queue, <exchange_name>, '');   //
// 5. Get message 
channel.consume(q.queue, function (msg) { 
  if (msg.content) console.log(msg.content.toString());
}, {
  noAck: true
})
```

`channel.bindingQueue(<queue>, <exchange_name>, <pattern>);`

because type exchange is **Fanout**, so <pattern> can be ' ', or something is the same, and it will not care


# Direct exchange

The producer will send the message to the consumer directly though the routing_key

### **Producer**

As the *Fanout*, producer will not care about the queue, it only *publish* the message to the exchange with the routing key. The exchange will help transfer the message to the queue and to the consumer.

Example:

```
// 1. connect to RabbitMQ server
const connect = await connectRabbit(...);

// 2. create a channel 
const channel = await connect.createChannel();

// 3. Check the exchange exists or not, and use the direct type
channel.assertExchange(<exchange_name>, 'direct', { durable: false }); // durable: alse <=> not store to the disk

// 4. Publish the message to the exchange
channel.publish(<exchange_name>, <routing_key>, <Buffer data>); // example: <routing_key>: log_message
```

### **Consumer**

As **Fanout**, consumer will define the exchange with the type **direct**, after that we bind the queue with the routing key to the exchange to listen the message though the matching routing key.

Example:
```
// 1. connect to RabbitMQ server
const connect = await connectRabbit(...);

// 2. create a channel
const channel = await connect.createChannel();

// 3. check the exchange exists
await channel.assertExchange(<exchange_name>, 'direct', { durable: false }); // durable: alse <=> not store to the

// 4. check the queue exists or not
const q = await channel.assertQueue('', { exclusive: false }); // exclusive: false <=> if not exists consumer => remove the queue

// 5. binding queue with the routing key
channel.bindQueue(q.queue, <exchange_name>, <routing_key>); // binding queue

// 6. consume message
channel.consume(q.queue, function (msg) {
  console.log(msg.content.toString()); // convert buffer to string
})
```

# Topic

As a direct key with exchange, producer will send the message to exchange, consumer will register the queue with the key to the exchange to get the message, but not as direct key, we use the topic (look like a regex)

- *(star): can substitude for exactly one word.
- #(hash): can substitude for zero or more words. 


**example**: 

Producer: publish the message to topic: **info.orange.rabbit**

Consumer: binding queue with the topic
 - \*.orange.\*: get the message 
 - \#.orange.\*: get the message


Producer: publish the message to topic: **orange.rabbit**

Consumer: binding queue with the topic
 - \*.orange.\*: not get message 
 - \#.orange.\*: get the message
 - orange.\#: get the message
 - orange.\*: not get the message
 - \#.orange: not get the message
 

### **Producer**

```
// connect to the rabbit server
// 1. connect to RabbitMQ server
const connect = await connectRabbit(...);

// 2. create a channel
const channel = await connect.createChannel();

// 3. check the exchange is exist or not

await channel.assertExchange(<exchange_name>, 'topic', { durable: false});
// durable: false <=> not store to disk

// 4. publish the message to the exchange with topic key
channel.publish(<exchange_name>, <topic_name>, Buffer.from(message));
```

### **Consumer**

```
// connect to RabbitMQ server
const connect = await connectRabbit();

// 2. create a channel
const channel = await connet.createChannel();

// 3. check the exchange is exist or not
await channel.assertExchange(<exchange_name>, 'topic', { durable: false });
// durable: false <=> not store to disk

// 4. check queue is exist or not
const q = await channel.assertQueue('', { exclusive: true });
// exclusive: true <=> remove queue if not get the message

// 5. bind queue to exchange
channel.bindQueue(q.queue, <exchange_name>, <topic_name>)
// <topic_name>: *.orange.# | quick.orange.rabbit | orange.* | orange.#

// 6. consume the message
channel.consume(q.queue, function (msg) {
  console.log(msg.toString());
})
```