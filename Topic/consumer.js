const connnectionRabbitMq = require('../ConnectRabbit');


const args = process.argv.slice(2);

const [topicRoutingKey] = args;

const exchangeName = 'exchange_topic';

async function consumer() {
  try {
    // 1. connect to rabbitmq server
    const connect = await connnectionRabbitMq();
    // 2. create a channel
    const channel = await connect.createChannel();
    // 3. check the exchange is exist or not
    await channel.assertExchange(exchangeName, 'topic', { durable: false });

    // 4. check the queue is exist or not
    const q = await channel.assertQueue('', { exclusive: false });
    // bind queue
    channel.bindQueue(q.queue, exchangeName, topicRoutingKey);

    // consume message
    channel.consume(q.queue, function (msg) {
      if (msg.content) {
        console.table({
          topic_routing_key: topicRoutingKey,
          msg: msg.content.toString()
        })
      }
    })

  } catch (err) {
    console.log(err)
  }
}

consumer();