const connnectionRabbitMq = require('../ConnectRabbit');

let args = process.argv.slice(2);

const [topicName, message] = args;
console.log(topicName, message);

const exchangeName = 'exchange_topic';

async function producer() {
  try {

    // 1. connect to rabbitmq server
    const connect = await connnectionRabbitMq();
    // 2. create a channel
    const channel = await connect.createChannel();
    // 3. check exchange 
    await channel.assertExchange(exchangeName, 'topic', { durable: false});

    channel.publish(exchangeName, topicName, Buffer.from(message));
    console.log('[Sent] msg : ' +  message);
    setTimeout(() => {
      process.exit(0);
    })


  } catch (err) {
    console.log(err)
  }
}


producer();

