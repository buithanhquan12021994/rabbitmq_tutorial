const connection = require('../ConnectRabbit');

let arguments = process.argv.slice(2);

const queue = 'workqueue';
let message = 'message_work_queue'

async function consumer() {
  try {
    const connect = await connection()

    connect.createChannel(function (err, channel) {
      if (err) throw err;
      channel.assertQueue(queue, { durable: true});
      channel.prefetch(1);
      channel.consume(queue, function (msg) {
        var secs = msg.content.toString().split('.').length - 1;
        console.log(secs);
        
        console.log(" [x] Received %s", msg.content.toString());
        setTimeout(function() {
          console.log(" [x] Done");
          channel.ack(msg); // thông báo cho rabbit rằng message ở chế độ acknowledgement đã xong
        }, secs * 1000);
      }, {
        noAck: false
      });
    })
  } catch (err) {

  }
}

consumer();