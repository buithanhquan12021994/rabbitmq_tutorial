const connection = require('../ConnectRabbit');

let arguments = process.argv.slice(2);

const queue = 'workqueue';
let message = process.argv.slice(2).join(' ') || 'Hello task queue'

async function procedure() {
  try {
    const connect = await connection()

    connect.createChannel(function (err, channel) {
      if (err) throw err;
      channel.assertQueue(queue, { durable: true});

      setInterval(() => {
        let numberdot = Math.floor(Math.random() * 30);
        let message = 'Here is dots'
        for (let i = 0; i <= numberdot; i ++) {
          message += '.'
        }
        console.log('msg: ' + message);

        channel.sendToQueue(queue, Buffer.from(message), {
          persistent: true, // nếu ko có persistent ==> task sẽ mất khi down rabbitmq
          durable: true,
        });
      }, 1000)
    })
  } catch (err) {

  }
}

procedure();