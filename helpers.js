const generateDot = (message) => {
  const numberRandom = Math.floor(Math.random() * 40);
  for (let i = 0; i <= numberRandom; i ++) {
    message += '.'
  }
  return message;
}

module.exports = {
  generateDot
}